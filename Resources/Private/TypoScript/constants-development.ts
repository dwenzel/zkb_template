# ******************************************************************************
#	(c) 2012 Georg Ringer <typo3@ringerge.org>
#
#	You can redistribute it and/or modify it under the terms of the
#	GNU General Public License as published by the Free Software Foundation;
#	either version 2 of the License, or (at your option) any later version.
# ******************************************************************************


# *******************************************************************
# Constants of the DEVELOPMENT mode
# !!! add everything to the production.ts and only those things
#     which differ to this section!
# *******************************************************************
[global]

plugin.theme_configuration {
	url = http://localhost:8333/
	noCache = 1

	general {
		googleanalytics = 0
		pageTitle.prefix = !!DEV!! Zentralkapelle -
	}

	navigation {
		#top = 17
		#footer = 77
		#login = 47
	}

	assets {
		merge = 0
		compress = 0
	}

	extensions {
		felogin {
			#storagePid = 48
		}
		
		#realurl = 1
		
		formhandler {
			email {
				user {
					sender_email = test@dev.zentralkapelle.de
				}
				admin {
					sender_email = test@dev.zentralkapelle.de
						to_email = der-wenzel@gmx.de
				}
			}
		}
	}
}

[globalString = ENV:HTTP_HOST = dev.zentralkapelle.de]
plugin.theme_configuration {
	url = http://dev.zentralkapelle.de/
	extensions {
	}
}
[global]
[globalString = ENV:HTTP_HOST = localhost:8333]
plugin.theme_configuration {
	navigation {
		footer = 55
	}
	extensions {
		placements {
			listPid = 42
			detailPid = 48
			position.searchPid = 64
		}
		indexed_search {
			page = 78
		}
	}
}
[global]
