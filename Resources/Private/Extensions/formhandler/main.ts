/**
 * _main.ts
 * includes extension TypoScript
 */

<INCLUDE_TYPOSCRIPT: source="FILE:EXT:zkb_template/Resources/Private/Extensions/formhandler/ContactForm/formhandler.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:zkb_template/Resources/Private/Extensions/formhandler/NewsletterSubscribe/formhandler.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:zkb_template/Resources/Private/Extensions/formhandler/NewsletterUnsubscribe/formhandler.ts">
