# special menu for front page (willkommen)
lib.frontNavi = HMENU
lib.frontNavi {
  special = list
  # Start-ID
  special.value = 4
  #special.value = 49
  includeNotInMenu = 1
#  entryLevel = 1
 
  wrap = <div id="frontNavi"> | </div>
 
  # Hauptmenue
  1 = TMENU
  1 {
#    wrap = <ul id="mainNavi" class="column"> | </ul>
    wrap = <ul> | </ul>
    expAll = 1
    NO.ATagTitle.field = subtitle//title
    #NO.wrapItemAndSub = <li><div class ="topItem"> | </div> </li>
    NO.wrapItemAndSub = <li class ="firstLevel"> | </li>
    NO.stdWrap.prepend = COA
    NO.stdWrap.prepend.10.wrap = ~&nbsp; |
    IFSUB = 1
    IFSUB {
      wrapItemAndSub = <li class ="firstLevel"> | </li>
      #allWrap = | <!--<![endif]-->
      #linkWrap = |<!--[if IE 7]><!-->
      ATagBeforeWrap = 1
    }
    ACT < .NO
    ACT = 1
    ACT.ATagParams = class="drop active"
    ACTIFSUB < .IFSUB
    ACTIFSUB.ATagParams = class="drop active sub"
  }

}