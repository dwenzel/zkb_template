lib.mainNaviCount = COA
lib.mainNaviCount {
	
}

lib.mainNavi = HMENU
lib.mainNavi {
  #excludeUidList = 64,15
 
  special = directory
  # Start-ID
  special.value = 1
  #special.value = 4
  #entryLevel = 1
#  wrap = <div id="mainNavi"> {register:count_HMENU_MENUOBJ}| </div>
  wrap = <div id="mainNavi">| </div>
 
 # wrap.insertData = 1
  # Hauptmenue
  1 = TMENU
  1 {
  	
#    wrap = <ul id="mainNavi" class="column"> | </ul>
    wrap = <ul> | </ul>
    expAll = 1
    NO {
#    	before = {register:count_HMENU_MENUOBJ}
 # 		beforeWrap = <div id="testMenueCount">|</div>
    	
    	ATagTitle.field = subtitle//title
    	wrapItemAndSub = <li><div class ="topItem"> | </div> </li>
    	wrapItemAndSub.insertData = 1
    	
    	stdWrap.prepend = COA
    	stdWrap.prepend.10.wrap = ~&nbsp; |
    }
    IFSUB = 1
    IFSUB {
      wrapItemAndSub = <li class ="firstLevel"> | </li>
      wrapItemAndSub.insertData = 1
      #allWrap = | <!--<![endif]-->
      #linkWrap = |<!--[if IE 7]><!-->
      ATagBeforeWrap = 1
    }
    ACT < .NO
    ACT = 1
    ACT.ATagParams = class="drop active"
    ACTIFSUB < .IFSUB
    ACTIFSUB.ATagParams = class="drop active sub"
  }
  2 = TMENU
  2 {
    #wrap = <!--[if lte IE 6]><table><tr><td><![endif]--><ul> | </ul><!--[if lte IE 6]></td></tr></table></a><![endif]-->
    wrap = <ul> | </ul>
    expAll = 1
    # normal state
    NO {
    	ATagTitle.field = subtitle//title
    	wrapItemAndSub = <li class="secondLevel"> | </li>
    	wrapItemAndSub.insertData = 1
    	}
    IFSUB = 1
    IFSUB {
      wrapItemAndSub = <li class="secondLevel sub"> | </li>
      #allWrap = | <!--<![endif]-->
      #linkWrap = |<!--[if IE 7]><!-->
      ATagBeforeWrap = 1
      ATagParams = class="drop"
    }
    #active state
    ACT < .NO
    ACT = 1
    ACT.ATagParams = class="active"
    ACTIFSUB < .IFSUB
    ACTIFSUB.ATagParams = class="active sub"
  }  

  3 < .2
  3{
    NO.wrapItemAndSub = <li class="thirdLevel"> | </li>
    IFSUB {
      wrapItemAndSub = <li class="thirdLevel sub"> | </li>
    	}
    #active state
    ACT < .NO
    ACT = 1
    ACT.ATagParams = class="active"
    ACTIFSUB < .IFSUB
    ACTIFSUB.ATagParams = class="active sub"
 	}
  
#  4 < .2
#  4{
#    NO.wrapItemAndSub = <li class="forthLevel"> | </li>
#    IFSUB {
#    	wrapItemAndSub = <li class="forthLevel sub"> | </li>
#     }
#   	}
}
