#################
# css files
#################
page.includeCSS{
	file1 = {$plugin.theme_configuration.path.css}/main.css
	file2 = {$plugin.theme_configuration.path.css}/mainNavi.css
	file3 = {$plugin.theme_configuration.path.css}/subNavi.css
	file4 = {$plugin.theme_configuration.path.css}/oneColumn.css
	file5 = {$plugin.theme_configuration.path.css}/twoColumns.css
	file6 = {$plugin.theme_configuration.path.css}/damFrontend.css
	file7 = {$plugin.theme_configuration.path.css}/default.css
	file8 = {$plugin.theme_configuration.path.css}/forms.css
  file9 = EXT:zkb_template/Resources/Public/Contrib/fancybox/jquery.fancybox.css
}
[browser = msie] AND [version = 7.0]
#	page.meta.http-equiv = IE=IE8
page.includeCSS.99 = {$plugin.theme_configuration.path.css}/ie7fix.css
[global]


#################
# js files
#################
#page.includeJS {
page.includeJSFooter {
   3 = EXT:zkb_template/Resources/Public/Contrib/fancybox/jquery.fancybox.js
   5 = {$plugin.theme_configuration.path.js}/custom.js
   50 = {$plugin.theme_configuration.path.js}/imageRotate.js
}

page.includeJSFooterlibs {
   jquery = {$plugin.theme_configuration.path.js}/jquery/1.7/jquery.min.js
   jquery.forceOnTop = 1
}
[globalVar = TSFE:id = 31]
page.includeJSFooter >
page.includeJSFooter {
	fullscreen = {$plugin.theme_configuration.path.js}/fullscreen.js
}
[global]

