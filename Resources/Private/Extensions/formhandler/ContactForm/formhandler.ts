# @file formhandler.ts
# @author Dirk Wenzel
# Provides a simple contact form 

plugin.Tx_Formhandler.settings.predef.ContactForm {

	name = Contact Form

	formValuesPrefix = contact

	langFile.1 = TEXT
	langFile.1.value = {$plugin.theme_configuration.path.ext}/formhandler/Common/Language/lang.xml

	templateFile = TEXT
	templateFile.value = {$plugin.theme_configuration.path.ext}/formhandler/ContactForm/Html/step-1.html

	masterTemplateFile = TEXT
	masterTemplateFile.value = {$plugin.theme_configuration.path.ext}/formhandler/Common/Html/mastertemplate.html

	requiredSign = TEXT
	requiredSign.value = *
	requiredSign.wrap = <span class="required">|</span>
	singleErrorTemplate {
		totalWrap = <div class="error formhandlererror">|</div>
		}

	validators {
		1.class = Validator_Default
		1.config.fieldConf {
			name.errorCheck.1 = required
			email.errorCheck.1 = required
			email.errorCheck.2 = email
			message.errorCheck.1 = required
		}
	}

	finishers {
		# Finisher_Mail sends emails to an admin and/or the user.
		1.class = Finisher_Mail
		1.config {
			checkBinaryCrLf = message
			admin {
				templateFile = TEXT
				templateFile.value = {$plugin.theme_configuration.path.ext}/formhandler/ContactForm/Html/email-admin.html
				sender_email = {$plugin.theme_configuration.extensions.formhandler.contact-form.email.admin.sender_email}
				to_email = {$plugin.theme_configuration.extensions.formhandler.contact-form.email.admin.to_email}
				subject = TEXT
				subject.data = LLL:{$plugin.theme_configuration.path.ext}/formhandler/Common/Language/lang.xml:contact_email_admin_subject
			}
			user {
				templateFile = TEXT
				templateFile.value = {$plugin.theme_configuration.path.ext}/formhandler/ContactForm/Html/email-user.html
				sender_email = {$plugin.theme_configuration.extensions.formhandler.contact-form.email.user.sender_email}
				to_email = email
				subject = TEXT
				subject.data = LLL:{$plugin.theme_configuration.path.ext}/formhandler/Common/Lang/lang.xml:contact_email_user_subject
			}
		}

		# Finisher_Redirect will redirect the user to another page after the form was submitted successfully.
		2.class = Finisher_SubmittedOK
		2.config.returns = 1
		
	}
	saveInterceptors {
		10.class = Interceptor_AntiSpamFormTime
		10.config {
			# ID of a page to redirect SPAM bots to
			redirectPage = 3
			minTime.value = 60
			minTime.unit = seconds
		}
	}
}

