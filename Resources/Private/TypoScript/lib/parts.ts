# fester Inhalt f�r social media links
temp.socialmedia = RECORDS
temp.socialmedia {
    tables=tt_content
    source=8
    dontCheckPid = 1
}
# fester Inhalt f�r social media links
temp.konzerttip = COA_INT
temp.konzerttip.10 = RECORDS
temp.konzerttip.10 {
    tables=tt_content
    source=9
    dontCheckPid = 1
}
temp.pageTitle = TEXT 
temp.pageTitle { 
	value = {page:title//page:nav_title} 
	insertData = 1 
	wrap = <div class="pageTitle"><h1>|</h1></div> 
}

# hide page title
#[globalVar = TSFE:id=53]
#temp.pageTitle >
#[global]

temp.andNow < styles.content.get
temp.andNow {
	#table = tt_content
	slide = -1
	select.where = colPos = 3
}

# Klassen f�r BODY-Tag vorbereiten
temp.layoutSwitch = CASE
temp.layoutSwitch {
   key.data = levelfield:-1,backend_layout_next_level,slide
   key.override.field = backend_layout
   default = TEXT
   default.value = threeColumns
   1 = TEXT
   1.value = fullsize
   4 = TEXT
   4.value = oneColumn
   3 = TEXT
   3.value = twoColumns
   6 = TEXT
   6.value = oneColumnCentered
   7 = TEXT
   7.value = fullsize
}
# BODY-Tag erstellen
temp.bodyTag = COA
temp.bodyTag {
   10 = TEXT
   10.dataWrap = <body id="p{TSFE:id}" class="
   10.noTrimWrap = | |
   # === insert layout switch (page|backend_layout)
	 #30 = TEXT
	 #30.value =  
   50 < temp.layoutSwitch
   90 = TEXT
   90.value = ">
   
}

temp.contentCenter = COA
temp.contentCenter {
	5 = TEXT
	5.value = <div id="contentWrap" 
	10 = TEXT
	10.dataWrap = style="width:{field:tx_extrapageprops_pagewidth}px; margin: 0 auto;"
	10.if {
	#10 nur ausgeben, wenn Feld tx_extrapageprops_pagewidth groesser als 450 ist
		value = 450
		isGreaterThan.data = field:tx_extrapageprops_pagewidth
		}
	15 = TEXT
	15.value = >
	
	20 = COA
	20{
		1 < temp.pageTitle
		2 < lib.subNavi
		wrap = <div id="subNaviTop" class="centered">|</div>
		}
	30 = COA
	30 {
		10 = TEXT
		10.value = <div id="content" class="item centered">
		20  < styles.content.get
		20.select.where = colPos = 1
		30 = TEXT
		30.value = </div>
		}
	100 = TEXT
	100.value= </div> 
}
# login
#temp.loginBox < plugin.tx_kcloginstatus_pi1
#temp.loginBox {
#	allWrap = <div id="loginBox"> | </div>
#  loginPID = 50
#  logoutPID = 50
#  loginWrap {
#  wrap = <p class="logIn">|</p><br />
#  }
#  logoutWrap {
#  wrap = <p class="logOut">|</p><br />
#  }
#  HTMLafterMessage = <br />
#  HTMLafterUsername = <br />
# }


# Loginformular
temp.login = COA
temp.login {
	wrap = <div id="login" class="closed"> | </div>
	10 = TEXT
	10.value = <form action="
	20 = TEXT
	20.typolink {
		parameter.data = TSFE:id
		returnLast = url
	} 
	30 = TEXT
	30.value = " enctype="multipart/form-data" method="post" class="loginform">
	40 = TEXT
	40.value = <input name="logintype" value="login" type="hidden" />
	50 = TEXT
	50.value = <input name="pid" value="{$pid_feuser}" type="hidden" />
	60 = TEXT
	60.value = <input name="user" size="20" value="Benutzername" onfocus="if(this.value=='Benutzername') this.value='';" onblur="if(this.value=='') this.value='Benutzername';" type="text" id="user" class="username" />
	70 = TEXT
	70.value = <input name="pass" size="20" value="Passwort" onfocus="if(this.value=='Passwort') this.value='';" onblur="if(this.value=='') this.value='Passwort';" type="password" id="pass" class="password" />
	80 = TEXT
	80.value = <input name="submit" value="Login" type="submit" class="submit" />
	100 = TEXT
	100.value = </form>
}
 
# Logoutbutton
temp.logout = COA_INT
temp.logout {
	wrap = <div id="login" class="open"> | </div>
  5 = TEXT
	5.typolink {
		parameter = {$pid_logout_redirect}
		returnLast = url
	} 
        5.wrap = <form action="|" class="loginform">
	10 = TEXT
	10.value = <input type="button" value="Logout" class="submit" onclick="location='
	20 = TEXT
	#20.wrap = {$baseURL}|
	20.typolink {
		value = abmelden
		parameter = {$pid_logout_redirect}
		returnLast = url
		additionalParams = &logintype=logout
	}
	30 = TEXT
	30.value = '" class="submit" />
	30.wrap = |<input type="hidden" name="logintype" value="logout" />
	40 = TEXT
	40.dataWrap = <div class="loggedInName">{TSFE:fe_user|user|name}</div>
	50 = TEXT
	50.value = </form>
}
[usergroup = *]
	temp.loginBox < temp.logout
[else]
	temp.loginBox < temp.login
[end]