content.defaultHeaderType = 2
lib.stdheader.10.3.dataWrap = <h2{register:headerStyle} class="csc-firstHeader marginBottom">|</h2>
# remove unneccessary tags
tt_content.stdWrap.dataWrap >
#max items for sitemap
tt_content.menu.20.6.maxItems = 3


#mailform
tt_content.mailform.20{
	REQ.labelWrap.wrap = |<span class="required">*</span>
}
# Disable csc-headers and styles output
# lib.stdheader.stdWrap.dataWrap >
#lib.stdheader.2.headerStyle >
#lib.stdheader.3.headerClass >

# remove default styles (put into default.css for editing)
plugin.tx_cssstyledcontent._CSS_DEFAULT_STYLE >

## unset margin of textpic (no-wrap) (use class 'textWrap' instead of image width)
tt_content.image.20{
	imageStdWrap.dataWrap = <div class="block csc-textpic-imagewrap"> | </div>
	rendering{
		simple.imageStdWrapNoWidth.wrap =<div class="block imageWrap csc-textpic-single-image"> | </div>
		div.imageRowStdWrap.dataWrap = <div class="csc-textpic-imagerow"> | </div>
		dl.imageRowStdWrap.dataWrap = <div class="csc-textpic-imagerow"> | </div>
		ul.imageRowStdWrap.dataWrap = <div class="csc-textpic-imagerow"><ul> | </ul></div>
		div.imageLastRowStdWrap.dataWrap =<div class="csc-textpic-imagerow csc-text-pic-imagerow-last"> | </div>
		dl.imageLastRowStdWrap.dataWrap =<div class="csc-textpic-imagerow csc-text-pic-imagerow-last"> | </div>
		ul.imageLastRowStdWrap.dataWrap =<div class="csc-textpic-imagerow csc-textpic-imagerow-last"><ul> | </ul></div>
		ul.oneImageStdWrap.dataWrap = <li class="csc-textpic-image###CLASSES###"> | </li>
	}
	layout{
		26.value =<div class="csc-textpic csc-textpic-intext-left-nowrap###CLASSES###">###IMAGES###<div class="block">###TEXT###</div></div><div class="csc-textpic-clear"><!-- --></div>
		25.value =<div class="csc-textpic csc-textpic-intext-right-nowrap###CLASSES###"><div class="block">###TEXT###</div>###IMAGES###</div><div class="csc-textpic-clear"><!-- --></div>
	}
	# add a light box
	1.imageLinkWrap {
		JSwindow = 0
		directImageLink = 1
		linkParams.ATagParams { 
			dataWrap = class = "fancybox" rel="fancybox{field:uid}"
		}
	}
}
# add class for layouts of content element
tt_content.stdWrap.innerWrap.cObject.default.15{
	noTrimWrap = | class="| layout-{field:layout}"|
	insertData = 1
}

	
#html tags erlauben
lib.parseFunc.allowTags = iframe,fieldset,label,textarea,dl,dt,dd,b,i,u,a,img,br,div,center,pre,font,sub,sup,p,strong,em,li,ul,ol,blockquote,strike,del,ins,span,h1,h2,h3,h4,h5,h6,address,script, form, input, select, option
lib.parseFunc_RTE.allowTags <lib.parseFunc.allowTags
#lib.parseFunc_RTE.allowTags =dl,dt,dd,b,i,u,a,img,br,div,center,pre,font,sub,sup,p,strong,em,li,ul,ol,blockquote,strike,del,ins,span,h1,h2,h3,h4,h5,h6,address,script, form, input
#page.10.allowTags = *
#page.10.preserveEntities = false
# Make sure nonTypoTagStdWrap operates on content outside <typolist> and <typohead> only:
lib.parseFunc.tags.typolist.breakoutTypoTagContent = 1
lib.parseFunc.tags.typohead.breakoutTypoTagContent = 1
lib.parseFunc_RTE.tags.typolist.breakoutTypoTagContent = 1
lib.parseFunc_RTE.tags.typohead.breakoutTypoTagContent = 1
# ... and no <BR> before typohead.
lib.parseFunc_RTE.tags.typohead.stdWrap.wrap >
lib.parseFunc_RTE.tags.typohead.stdWrap.wrap >

###
#lib.parseFunc.nonTypoTagStdWrap.encapsLines>
#lib.parseFunc_RTE.nonTypoTagStdWrap.encapsLines>
lib.parseFunc.nonTypoTagStdWrap.encapsLines {
  #encapsTagList = p,div
  #remapTag.DIV = P
  #remapTag.P = p
	#wrapNonWrappedLines = |
  #wrapNonWrappedLines = <p>|</p>
#  innerStdWrap_all.ifEmpty = 
# p tags erhalten aber attribute entfernen
	addAttributes {
    P.style=
    PRE.style=
    }
}
lib.parseFunc_RTE.nonTypoTagStdWrap.encapsLines < lib.parseFunc.nonTypoTagStdWrap.encapsLines
#lib.parseFunc.nonTypoTagStdWrap.HTMLparser = 0
#lib.parseFunc_RTE.nonTypoTagStdWrap.HTMLparser = 0
#lib.parseFunc.nonTypoTagStdWrap.HTMLparser.htmlSpecialChars = 0
#lib.parseFunc_RTE.nonTypoTagStdWrap.HTMLparser.htmlSpecialChars =0
#tt_content.header.20.htmlSpecialChars = 0

##
