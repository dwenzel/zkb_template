config {
	compressCSS = {$plugin.theme_configuration.assets.compress}
	concatenateCss = {$plugin.theme_configuration.assets.merge}
	concatenateJs = {$plugin.theme_configuration.assets.merge}
	no_cache= {$plugin.theme_configuration.noCache}
}
page.config {
	simulateStaticDocuments = 0
  #baseURL = http://www.zentralkapelle.de/
  baseURL = {$plugin.theme_configuration.url}
  #tx_cooluri_enable = 1
	tx_realurl_enable = {$plugin.theme_configuration.extensions.realurl}
  #Optional alte gespeicherte Links zu neuen Weiterleiten
  #redirectOldLinksToNew = 1
	
	# disable standard  page title (reset in page.ts)
  noPageTitle = 1
}
config {
	doctype= xhtml_trans
	htmlTag_langKey = de_DE
	removeDefaultJS = external
	xhtml_cleaning = all
	disablePrefixComment = 1
}
 # disable xml prolog to prevent quirks-mode in IE
[browser = msie]
config.xmlprologue = none
#config.doctypeSwitch = 1
#[global] 

[browser = msie] AND [version = 8.0]
#	page.meta.http-equiv = IE=IE8
page.headTag = <head><meta http-equiv="X-UA-Compatible" content="IE=8" />
[global]

####################
# Extensions
####################
#ke_stats (Statistik)
#page.headerData.100 < plugin.tx_kestats_pi1

page.stdWrap.HTMLparser {
	keepNonMatchedTags = 1
	# remove/keep target attribute in links (xhtml conform)
	tags.a.fixAttrib.target.unset = 0
}

