<?php

########################################################################
# Extension Manager/Repository config file for ext "a21glossary".
#
# Auto generated 17-02-2012 15:52
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'ZKB Template',
	'description' => 'Template provider for Zentralkapelle.de',
	'category' => 'fe',
	'shy' => 0,
	'dependencies' => 'extbase,fluid',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => 0,
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Dirk Wenzel',
	'author_email' => 'dirk.wenzel@sinnzeichen.de',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'version' => '0.1.0',
	'_md5_values_when_last_written' => '',
	'constraints' => array(
		'depends' => array(
			'php' => '5.1.0-0.0.0',
			'typo3' => '4.6.0-0.0.0',
			'fluid' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
			'news' => '1.3.0-',
		),
	),
	'suggests' => array(
	),
);

?>
