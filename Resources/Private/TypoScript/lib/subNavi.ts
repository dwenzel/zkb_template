lib.subNavi = HMENU
lib.subNavi {
  #excludeUidList = 64,15
 
  #special = directory
  # Start-ID
  #special.value = 1
  entryLevel = 2
  excludeUidList = current
  wrap = <div id="subNavi"> | </div><div class="clearFloat"></div>
 
  # Hauptmenue
  1 = TMENU
  1 {
    wrap = <ul> | </ul>
    expAll = 1
    NO.ATagTitle.field = subtitle//title
    NO.wrapItemAndSub = <li><div class ="forthLevel"> | </div> </li>
    NO.stdWrap.prepend = COA
    NO.stdWrap.prepend.10.wrap = ~&nbsp; |
    IFSUB = 1
    IFSUB {
      wrapItemAndSub = <li class ="forthLevel"> | </li>
      #allWrap = | <!--<![endif]-->
      #linkWrap = |<!--[if IE 7]><!-->
      ATagBeforeWrap = 1
    }
    ACT < .NO
    ACT = 1
    ACT.ATagParams = class="drop active"
    ACTIFSUB < .IFSUB
    ACTIFSUB.ATagParams = class="drop active sub"
  }
/*
  2 = TMENU
  2 {
    #wrap = <!--[if lte IE 6]><table><tr><td><![endif]--><ul> | </ul><!--[if lte IE 6]></td></tr></table></a><![endif]-->
    wrap = <ul> | </ul>
    expAll = 1
    # normal state
    NO.ATagTitle.field = subtitle//title
    NO.wrapItemAndSub = <li class="secondLevel"> | </li>
    IFSUB = 1
    IFSUB {
      wrapItemAndSub = <li class="secondLevel sub"> | </li>
      #allWrap = | <!--<![endif]-->
      #linkWrap = |<!--[if IE 7]><!-->
      ATagBeforeWrap = 1
      ATagParams = class="drop"
    }
    #active state
    ACT < .NO
    ACT = 1
    ACT.ATagParams = class="active"
    ACTIFSUB < .IFSUB
    ACTIFSUB.ATagParams = class="active sub"
  }  

  3 < .2
  3{
    NO.wrapItemAndSub = <li class="thirdLevel"> | </li>
    IFSUB {
      wrapItemAndSub = <li class="thirdLevel sub"> | </li>
    	}
    #active state
    ACT < .NO
    ACT = 1
    ACT.ATagParams = class="active"
    ACTIFSUB < .IFSUB
    ACTIFSUB.ATagParams = class="active sub"
 	}
  
  4 < .2
  4{
    NO.wrapItemAndSub = <li class="forthLevel"> | </li>
    IFSUB {
    	wrapItemAndSub = <li class="forthLevel sub"> | </li>
     }
   	}
   	*/
}
