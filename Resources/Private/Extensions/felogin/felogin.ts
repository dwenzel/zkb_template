plugin.tx_felogin_pi1 {
		#storagePid - where are the user records? use single value or a commaseperated list
#	storagePid = {$plugin.theme_configuration.extensions.felogin.storage.pid}
	#storagePid = 4

	#recursive = 
		#Template File
	#templateFile = {$plugin.theme_configuration.path.ext}/felogin/template.html

		#baseURL for the link generation
	#feloginBaseURL =

		#wrapContentInBaseClass
	wrapContentInBaseClass = 1


		#typolink-configuration for links / urls
		#parameter and additionalParams are set by extension
	linkConfig {
		target =
		ATagParams = rel="nofollow"
	}

		#preserve GET vars - define "all" or commaseperated list of GET-vars that should be included by link generation
	preserveGETvars = all


		#additional fields
	showForgotPasswordLink = 1
	showPermaLogin =

		# time in hours how long the link for forget password is valid
	forgotLinkHashValidTime = 12

	newPasswordMinLength = 10

	welcomeHeader_stdWrap {
		wrap = <h3>|</h3>
	}
	welcomeMessage_stdWrap {
		wrap = <div class="message">|</div>
	}

	successHeader_stdWrap {
		wrap = <h3>|</h3>
	}
	successMessage_stdWrap {
		wrap = <div class="message success">|</div>
	}

	logoutHeader_stdWrap {
		wrap = <h3>|</h3>
	}
	logoutMessage_stdWrap {
		wrap = <div class="message success">|</div>
	}

	errorHeader_stdWrap {
		wrap = <h3>|</h3>
	}
	errorMessage_stdWrap {
		wrap = <div class="message error">|</div>
	}

	forgotHeader_stdWrap {
		wrap = <h3>|</h3>
	}
	forgotMessage_stdWrap {
		wrap = <div class="message">|</div>
	}
	forgotErrorMessage_stdWrap {
		wrap = <div class="message error">|</div>
	}
	forgotResetMessageEmailSentMessage_stdWrap {
		wrap = <div class="message success">|</div>
	}
	changePasswordNotValidMessage_stdWrap {
		wrap = <div class="message error">|</div>
	}
	changePasswordTooShortMessage_stdWrap {
		wrap = <div class="message error">|</div>
	}
	changePasswordNotEqualMessage_stdWrap {
		wrap = <div class="message error">|</div>
	}

	changePasswordHeader_stdWrap {
		wrap = <h3>|</h3>
	}
	changePasswordMessage_stdWrap {
		wrap = <div class="message">|</div>
	}
	changePasswordDoneMessage_stdWrap {
		wrap = <div class="message success">|</div>
    }

	cookieWarning_stdWrap {
		wrap = <p style="color:red; font-weight:bold;">|</p>
	}

	# stdWrap for fe_users fields used in Messages
	userfields {
		username {
			htmlSpecialChars = 1
			wrap = <strong>|</strong>
		}
	}

		#redirect
	redirectMode =
	redirectFirstMethod =
	redirectPageLogin =
	redirectPageLoginError =
	redirectPageLogout =

	#disable redirect with one switch
	redirectDisable =

	email_from =
	email_fromName =
	replyTo =


	# Allowed Referrer-Redirect-Domains:
	domains =

	# Show logout form direct after login
	showLogoutFormAfterLogin =

	dateFormat = Y-m-d H:i

	# Expose the information on whether or not the account for which a new password was requested exists. By default, that information is not disclosed for privacy reasons.
	exposeNonexistentUserInForgotPasswordDialog = 0
}

plugin.tx_felogin_pi1._CSS_DEFAULT_STYLE (
	.tx-felogin-pi1 label {
		display: block;
	}
)

plugin.tx_felogin_pi1._LOCAL_LANG {
	default {
	}
	de {
		username = E-Mail:
		password = Kennwort:
		ll_forgot_header = Kennwort vergessen oder in persönliches ändern
		ll_forgot_message = Bitte geben Sie Ihre E-Mail-Adresse ein mit der Sie sich an der Stellenbörse registriert haben. Sie erhalten dann umgehend eine E-Mail mit Anweisungen zum Ändern des Kennworts zugesandt.
		reset_password = Kennwort Ändern
		ll_forgot_reset_message_emailSent = Es wurde eine E-Mail mit einem Link zum Ändern des Kennworts an die in Ihrem Benutzerkonto gespeicherte E-Mail-Adresse geschickt. Sollten Sie keine E-Mail erhalten, wurde Ihr Konto oder die E-Mail-Adresse nicht gefunden.
	}
}

