page = PAGE
page{
	typeNum = 0
	meta.DESCRIPTION = Zentralkapelle Berlin
	meta.KEYWORDS = Zentralkapelle, Berlin, Sinfonisches Blasorchester, Orchester
	#shortcutIcon = {$plugin.theme_configuration.path.img}/favicon.ico
	shortcutIcon = {$plugin.theme_configuration.path.icon}/favicon.ico
	# reset page title
	headerData.6 = TEXT
  headerData.6 {
  field = subtitle//title
	wrap = <title> Zentralkapelle Berlin &#124; &nbsp; | </title>
	}
	
}

#bodyTag neu setzten
page.bodyTag >
page.bodyTagCObject < temp.bodyTag

# Create a Fluid Template
page.10 = FLUIDTEMPLATE
page.10 {
  # Set the Template Pathes
  partialRootPath = {$plugin.theme_configuration.path.template}/Partials/
  layoutRootPath = {$plugin.theme_configuration.path.template}/Layouts/
  variables {
    content_left < styles.content.get
    content_left.select.where = colPos = 1
    content_right < styles.content.get
    content_right.select.where = colPos = 2
    contentCenter < temp.contentCenter    
    and_now < temp.andNow
    #and_now < styles.content.get
    #and_now.select.where= colPos = 3
    footer < temp.socialmedia
    pageTitle < temp.pageTitle
    #footer < styles.content.get
    #footer.select.where = colPos = 4
    tip < temp.konzerttip
    #tip < styles.content.get
    #tip.select.where = colPos =5

		# login box
		loginBox < temp.loginBox
  }
}

# Assign the Template files with the Fluid Backend-Template
page.10.file.stdWrap.cObject = CASE
page.10.file.stdWrap.cObject {
  key.data = levelfield:-1, backend_layout_next_level, slide
  key.override.field = backend_layout

  # Set the default template
  default = TEXT
  default.value = {$plugin.theme_configuration.path.template}/Layouts/twoColumns.html

  
  # Set first [1] template (full size)
  1 = TEXT
  1.value = {$plugin.theme_configuration.path.template}/Layouts/fullSize.html

	# set second [2] template (one column)
  4 = TEXT
  4.value = {$plugin.theme_configuration.path.template}/Layouts/oneColumn.html
  
  6 = TEXT
  6.value = {$plugin.theme_configuration.path.template}/Layouts/oneColumnCentered.html
  
  # Set third  [3] template (3 columns)
  3 = TEXT
  3.value = {$plugin.theme_configuration.path.template}/Layouts/twoColumns.html
  
	# alternate template for front page [4]
  7 = TEXT
  7.value = {$plugin.theme_configuration.path.template}/Layouts/fullSizeHint.html
}
