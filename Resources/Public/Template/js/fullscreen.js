jQuery.noConflict();

jQuery(document).ready(
function($) {
    /**
     * jQuery-Funktionenerweiterung
     */
    $.extend({
        /**
         * Hintergrundbild auf 100% setzen
         */
        bgImage : function() {
    	var container = $('#content_main_full');
    	//var containerRatio = container.width()/container.height();
    	
            $('#content_main_full .csc-textpic a>img').each(function() {
            	$this = $(this);
                //$this.fadeTo('slow', 0);
            	//console.log('container width, height, ratio:', container.width(), container.height(), container.width()/container.height());
                $this.width(container.width()).height('auto');
            	/*if (containerRatio <=1){
                	$this.width(container.width()).height('auto');
                }
                else {
                	$this.height(container.height()).width('auto');
                }*/
/*            	if ($this.width() > $this.height()) {
                    $this.width($(window).width()-215).height('auto');
                } else {
                    $this.height($(window).height()-214).width('auto');
                }

                if ($this.height() < $(window).height()) {
                    var m = ($this.height() / 2)-100;
                    $this.css({
                        //position : 'absolute',
                        top : '50%',
                        //marginTop : '-' + m + 'px'
                    });
                }*/
                $this.fadeTo('slow', 1);
            });
        }
    });

    window.onload = function() {
        // Hintergrundbild dem Fenster anpassen
        window.onresize = function() {
            $.bgImage();
        }

        $.bgImage();
    };

    /**
     * Externe Links im neuen Fenster oeffnen
     */
    $('a[href^="http"]').click(function() {
        var nw = window.open(this.href, '_blank');
        if (nw) {
            if (nw.focus) {
                nw.focus();
            }
        }
        nw = null;
        return false;
    });
});
