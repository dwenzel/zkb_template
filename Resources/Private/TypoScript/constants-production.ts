# ******************************************************************************
#	(c) 2012 Georg Ringer <typo3@ringerge.org>
#
#	You can redistribute it and/or modify it under the terms of the
#	GNU General Public License as published by the Free Software Foundation;
#	either version 2 of the License, or (at your option) any later version.
# ******************************************************************************


#-------------------------------------------------------------------------------
#	General Constants
#-------------------------------------------------------------------------------
#styles.content.imgtext.maxW = 1200

#-------------------------------------------------------------------------------
#	Constants of the PRODUCTION mode
#-------------------------------------------------------------------------------
plugin.theme_configuration {
	noCache = 0
	# Domain fur Produktiv-Betrieb
	# auf dem Live-System wird eine Arbeitsdomain verwendet,
	url = http://www.zentralkapelle.de/
	path {
		template = EXT:zkb_template/Resources/Private/Templates
		css = EXT:zkb_template/Resources/Public/Template/css
		js = EXT:zkb_template/Resources/Public/Template/js
		img = EXT:zkb_template/Resources/Public/Template/images
		icon = EXT:zkb_template/Resources/Public/Template/icons
		ext = EXT:zkb_template/Resources/Private/Extensions
	}

	general {
		#adminPanel = 1
		#copyright_information.link = 242
		#googleanalytics = 1
		#googleanalytics.code = UA-XXXXXXXXXXX

		pageTitle {
			prefix = Zentralkapelle Berlin | 
			suffix =
		}
	}
	
	show {
		staticLogo = 0
		signpost = 1
		searchField = 1
		teaserBoxes = 0
		logoutButton = 0
		socialmedia = 1
	}

	assets {
		merge = 1
		compress = 1
	}

	navigation {
		# home pid
		#home = 26
		#top = 17
		#footer = 72
		# login page id
		#login = 50
		# pid for teaser menu home 
		#teaserHome = 108 
	}

	extensions {
		
		
		felogin {
			#storagePid = 52
		}
		
		realurl = 1

		indexed_search {
			#page = 147
		}
		

		formhandler {
			newsletterSubscription{
				adminMail = bjanowski@web.de
				senderMail = newsletter@zentralkapelle.de
				redirectPage = 56
				errorRedirectPage = 91
			}
			
			newsletterUnsubscribe{
				adminMail = bjanowski@web.de
				senderMail = newsletter@zentralkapelle.de
				redirectPage = 58
				errorRedirectPage = 91
			}
			
		}
	}
}

[globalString = ENV:HTTP_HOST = dev.zentralkapelle.de]
plugin.theme_configuration.url = http://dev.zentralkapelle.de/
[global]

[globalString = ENV:HTTP_HOST = www.zentralkapelle.de]
plugin.theme_configuration {
	url = http://zentralkapelle.de
	general {
		pageTitle {
			#prefix >
		}
	}
}
[global]

