# @file formhandler.ts
# @author Dirk Wenzel
# Provides a simple newsletter subscription form 

plugin.Tx_Formhandler.settings.predef.NewsletterSubscribeForm {
	#debug = 1
	#debuggers.1.class = Tx_Formhandler_Debugger_Print
	name = Newsletter Subscribe Form

	formValuesPrefix = subscribe

	langFile.1 = TEXT
	langFile.1.value = {$plugin.theme_configuration.path.ext}/formhandler/Common/Language/lang.xml

	templateFile = TEXT
	templateFile.value = {$plugin.theme_configuration.path.ext}/formhandler/NewsletterSubscribe/Html/step-1.html

	masterTemplateFile = TEXT
	masterTemplateFile.value = {$plugin.theme_configuration.path.ext}/formhandler/Common/Html/mastertemplate.html

	requiredSign = TEXT
	requiredSign.value = *
	requiredSign.wrap = <span class="required">|</span>
	singleErrorTemplate {
		totalWrap = <div class="error formhandlererror">|</div>
		}

	validators {
		1.class = Validator_Default
		1.config.fieldConf {
			name.errorCheck.1 = required
			email.errorCheck.1 = required
			email.errorCheck.2 = email
			#message.errorCheck.1 = required
		}
	}

	finishers {
		# Finisher_Mail sends emails to an admin and/or the user.
		1.class = Finisher_Mail
		1.config {
			checkBinaryCrLf = message
			admin {
				templateFile = TEXT
				templateFile.value = {$plugin.theme_configuration.path.ext}/formhandler/NewsletterSubscribe/Html/email-admin.html
				sender_email = {$plugin.theme_configuration.extensions.formhandler.newsletterSubscription.senderMail}
				to_email = {$plugin.theme_configuration.extensions.formhandler.newsletterSubscription.adminMail}
				subject = TEXT
				subject.data = LLL:{$plugin.theme_configuration.path.ext}/formhandler/Common/Language/lang.xml:newsletter_subscribe_email_admin_subject
			}
			user {
				templateFile = TEXT
				templateFile.value = {$plugin.theme_configuration.path.ext}/formhandler/NewsletterSubscribe/Html/email-user.html
				sender_email = {$plugin.theme_configuration.extensions.formhandler.newsletterSubscription.senderMail}
				to_email = email
				subject = TEXT
				subject.data = LLL:{$plugin.theme_configuration.path.ext}/formhandler/Common/Language/lang.xml:newsletter_subscribe_email_user_subject
			}
		}

		# Finisher_Redirect will redirect the user to another page after the form was submitted successfully.
		2.class = Finisher_Redirect
		2.config {
			redirectPage = 56
		}
		#2.class = Finisher_SubmittedOK
		#2.config {
		#	returns = 1
		#}
	}
	saveInterceptors {
		10.class = Interceptor_AntiSpamFormTime
		10.config {
			# ID of a page to redirect SPAM bots to
			redirectPage = 91
			minTime.value = 3
			minTime.unit = seconds
		}
	}
}

